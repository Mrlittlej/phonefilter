//
//  ViewController.swift
//  HelloFilter
//
//  Created by XueXin Tsai on 2016/5/8.
//  Copyright © 2016年 XueXin Tsai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let imagePicker = UIImagePickerController()
    var image:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imagePicker.delegate = self
        
    }

    @IBAction func takePicture(sender: UIButton) {
        
    }
    
    @IBAction func pickFromAlbum(sender: UIButton) {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(self.imagePicker, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let imageVC = segue.destinationViewController as! ImageViewController
        imageVC.image = self.image
    }
}

extension ViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        //
        
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        
        if let aImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.image = aImage
        }
        
        self.performSegueWithIdentifier("Image", sender: nil)
    }
    
}

